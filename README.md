# Simplification de termes

## Installation
Pour installer le projet:

    npm install

Pour l'utiliser: le site web est défini dans dist, il suffit de le copier
dans l'arborescence d'un serveur ou de lancer un serveur à cet endroit:

    python -m SimpleHTTPServer

## Utilisation
Taper une formule dans la boite et presser sur "Analyse". La version actuelle
comprend les quatre opérations.

La formule doit
apparaître au format Latex en dessous et sous forme d'arbre dans la fenêtre
de réécriture. Ce nouvel arbre est la formule en cours de réécriture sur
laquelle on travaille.

Pour réécrire, selectionner le membre d'une équation (droit ou gauche) puis
un noeud de la formule en cours de réécriture qui est compatible avec la règle.
On peut aussi simplifier un calcul en appuyant sur "Calcul" qui change le
mode de travail. En appuyant sur un noeud corespondant à un sous arbre
**sans variable**, une fenêtre s'ouvre dans laquelle
on peut taper une nouvelle formule **sans variable**. Si la formule a la même
valeur que le sous arbre, elle remplace le sous arbre.
