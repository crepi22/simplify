import * as $ from "jquery";
import "bootstrap";
import {nameIt} from "./ast";
import {parse, SyntaxError} from "./parser";
import {Rewriter} from "./rewrite";
import { TreeView } from "./tree";

const rules: string[] = [
  "x+y = y+x",
  "x+0 = x",
  "x-x = 0",
  "x+(y+z) = (x+y)+z",
  "x*y = y*x",
  "x*0 = 0",
  "x*1 = x",
  "x/x = 1",
  "x*(y*z) = (x*y)*z",
  "x*(y+z) = x*y + x*z",
]

function main() {
    const view = new TreeView();
    const rewrites = new Rewriter(view, rules);
    view.setNodeCallback((x) => rewrites.select(x.data))
    rewrites.display();
    function action() {
        this.blur();
        const c = $("#parse-input").val();
        try {
            if (typeof(c) === "string") {
                const tree = parse(c);
                nameIt(tree, "FO");
                const val = JSON.stringify(tree);
                $("#parsed").text(val);
                rewrites.setFormula(tree);
            }
        } catch (e) {
            $("#parsed").text("Syntax error" + e);
        }
    }
    $("#parse-button").click(action);
    $("#compute-button").click(function (ev){this.blur(); rewrites.computeMode()});
    $("#result-button").click(ev => {
        const result = $("#result-input").val();
        $("#result-input").val("");
        if (typeof(result) !== "string") return;
        rewrites.checkResult(result);
    });
}

$(document).ready(main);
