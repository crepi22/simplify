import { select, Selection, BaseType } from "d3-selection";
import * as hierarchy from "d3-hierarchy";
import "d3-transition";
import { AST, Kind } from "./ast";


const width = 800;
const height = 400;

export interface Layout {
    ast: AST,
    bbox: DOMRect,
    name: string,
    tree: Selection<SVGGElement, any, HTMLElement, any>,
    x: number,
    y: number
}

function astClass(ast: AST): string {
    switch (ast.kind) {
        case Kind.Call:
            return "node-call";
        case Kind.Const:
            return "node-const";
        case Kind.Var:
            return "node-var";
    }
}
export class TreeView {
    svg: Selection<SVGSVGElement, any, HTMLElement, any>;
    click: (d:hierarchy.HierarchyPointNode<AST>) => void;

    constructor() {
        this.svg = select("#svgout").append("svg").attr("width", width).attr("height", height);
        this.click = (d:hierarchy.HierarchyPointNode<AST>) => console.log(d.data);
    }

    position(layout: Layout, x: number, y: number) {
        layout.tree.attr("transform", `translate(${x - layout.bbox.x},${y - layout.bbox.y})`);
        layout.x = x - layout.bbox.x;
        layout.y = y - layout.bbox.y;
    }

    drawAt(text: string, x: number, y: number) {
        this.svg.append("text").style("text-anchor","middle")
            .style("dominant-baseline","central")
            .text(text).attr("transform", `translate(${x},${y})`);
    }

    computeLayout(data: AST): Layout {
        const self = this;
        const name = data.from;
        if (!name) throw Error("unamed layout");
        this.svg.selectAll(`#${name}`).remove();
        const g = this.svg.append("g").attr("id", name)
        const nodeSize =  <[number, number]>[30, 20];
        const root = hierarchy.hierarchy<AST>(data, (d) => d.children);
        const tree = hierarchy.tree<AST>().nodeSize(nodeSize);
        const nodes = tree(root);
        const link = g.selectAll<SVGPathElement, hierarchy.HierarchyPointNode<AST>>(".link")
            .data(nodes.descendants().slice(1)).enter()
            .append("path").attr("class","link")
            .attr("d", (d) => `M ${d.x},${d.y} L ${d.parent ? d.parent.x: 0},${d.parent ? d.parent.y : 0}` );
        const node = g.selectAll(".node").data(nodes.descendants())
            .enter().append("g")
            .on("click", function(d) {
                effect(select(this), "blue", 500);
                self.click(d)})
            .attr("class", d => `node ${astClass(d.data)}`)
            .attr("id", d => `I${d.data.id}`)
            .attr("transform", d => `translate(${d.x},${d.y})`);
        node.append("circle").attr("r", 7);
        node.append("text").style("text-anchor","middle")
            .style("dominant-baseline","central")
            .text(d => "" + d.data.val);
        const theg = g.node()
        if (!theg) throw new Error("No bounding box")
        const box = theg.getBBox();
        const x = 400 - box.x;
        const y = 300 - box.y;
        g.append("rect").attr("x", box.x).attr("y", box.y).attr("width", box.width)
            .attr("height", box.height).attr("stroke","black")
            .attr("fill","none").attr("stroke-width",1).attr("opacity", 0);
        g.attr("transform", `translate(${x},${y})`);
        return {
            name: name,
            ast: data,
            bbox: box,
            tree: g,
            x: x,
            y: y,
        }
    }

    setNodeCallback(cb: (d: hierarchy.HierarchyPointNode<AST>) => void) {
        this.click = cb;
    }
}

export function highlight(layout: Layout, opacity: number) {
    layout.tree.select("rect").attr("opacity", opacity);
}

export function effect(
    sel: Selection<any, any, any, any>,
    color: string, duration: number,
) {
    sel.select("circle").style("fill",color)
        .transition().duration(duration).style("fill","aliceblue");
}
