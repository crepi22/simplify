import {AST, Kind} from "./ast";

export function compute(term: AST): number {
    switch (term.kind) {
        case Kind.Var:
            throw new Error("Je ne réduit pas les variables");
        case Kind.Const:
            if (typeof(term.val) !== "number") {
                throw new Error("Mauvaise constante");
            } else {
                return term.val;
            }
        case Kind.Call:
            if (term.children.length === 2) {
                const t0 = compute(term.children[0]), t1 = compute(term.children[1]);
                switch (term.val) {
                    case "+":
                        return t0 + t1;
                    case "-":
                        return t0 - t1;
                    case "*":
                        return t0 * t1;
                    case "/":
                        return t0 / t1;
                    default:
                        throw new Error(`Primitive ${term.val} inconnue`);
                }
            } else if (term.children.length === 1) {
                const t0 = compute(term.children[0]);
                switch (term.val) {
                    case "-":
                        return -t0;
                    default:
                        throw new Error(`Primitive ${term.val} inconnue`);
                }
            } else throw new Error(`Arité étrange ${term.children.length}`);
    }
}