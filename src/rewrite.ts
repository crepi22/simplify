import * as $ from "jquery";
import { parse } from "./parser";
import { Layout, TreeView, effect, highlight } from "./tree";
import { AST, Kind, eqTerm, nameIt, copy, binop, variable } from "./ast";
import { latex, render } from "./pretty";
import { compute } from "./compute";

export interface RewriteRule {
    left: Layout,
    right: Layout,
}

export class Rewriter {
    private view: TreeView;
    private rewrites: string[];
    private formula: AST;
    private toReduce: AST | null = null;
    private current: RewriteRule | null = null;
    private columns: number;
    private direct: boolean;
    private highlighted: Layout | null = null;
    public rewriteRules: RewriteRule[];
    private layout: Layout;

    constructor(view: TreeView, rewrites: string[], columns = 3) {
        this.rewrites = rewrites;
        this.view = view;
        this.columns = columns
    }

    display() {
        const columns = this.columns;
        let c = 0;
        const result: RewriteRule[] = [];
        let maxLeft = new Array(columns).fill(0),
            maxRight = new Array(columns).fill(0);
        for (const rw of this.rewrites) {
            try {
                const ast: AST = parse(rw);
                if (ast.kind !== Kind.Call || ast.val !== "=") {
                    console.log(`Cannot parse rule: ${rw}`)
                    continue;
                }
                const [l_ast, r_ast] = ast.children;
                nameIt(l_ast, `L${c}`);
                const left = this.view.computeLayout(l_ast);
                maxLeft[c % columns] = Math.max(maxLeft[c % columns], left.bbox.width);
                nameIt(r_ast, `R${c}`);
                const right = this.view.computeLayout(r_ast);
                maxRight[c % columns] = Math.max(maxRight[c % columns], right.bbox.width);
                const rewrite = { left: left, right: right };
                result.push(rewrite);
                c++;
            } catch (e) {
                console.log(`Cannot parse rule: ${rw}`);
                console.log(e);
            }
        }
        let posLeft = new Array(columns), posRight = new Array(columns);
        for (let i = 0; i < columns; i++) {
            posLeft[i] = (i === 0) ? 20 : posRight[i - 1] + maxRight[i - 1] + 40;
            posRight[i] = posLeft[i] + maxLeft[i] + 20;
        }
        let h = 20;
        let i = 0;
        let delta = 0
        for (const rewrite of result) {
            const hrw = Math.max(rewrite.left.bbox.height, rewrite.right.bbox.height);
            delta = Math.max(delta, hrw);
            const hdl = (hrw - rewrite.left.bbox.height) / 2;
            const hdr = (hrw - rewrite.right.bbox.height) / 2;
            this.view.position(
                rewrite.left,
                posLeft[i] + maxLeft[i] - rewrite.left.bbox.width,
                h + hdl);
            this.view.drawAt('=', posRight[i] - 10, h + hrw / 2);
            this.view.position(rewrite.right, posRight[i], h + hdr);
            i = (i + 1) % columns;
            if (i === 0) {
                h += delta + 10;
                delta = 0;
            }
        }
        this.rewriteRules = result;
    }

    setFormula(formula: AST) {
        this.formula = formula;
        render('formula', formula);
        this.layout = this.view.computeLayout(formula);
    }

    select(term: AST) {
        console.log("SELECT");
        console.log(term);
        const from = term.from;
        if (from === undefined) {
            return;
        } else if (from === 'FO') {
            if (this.current != null) {
                const matcher =
                    this.direct ? this.current.left : this.current.right;
                const subst = new Map<string, AST>();
                if (this.match(term, matcher.ast, subst)) {
                    const substituted =
                        this.direct ? this.current.right : this.current.left;
                    const replacement = copy(substituted.ast, subst);
                    term.kind = replacement.kind;
                    term.val = replacement.val;
                    term.children = replacement.children;
                    nameIt(this.formula, "FO");
                    this.setFormula(this.formula);
                } else {
                    this.error(
                        "Je ne peux pas appliquer la règle à cet endroit.");
                }
            } else {
                this.toReduce = term;
                $("#compute-modal").modal()
            }
        } else if (from.charAt(0) === 'L' || from.charAt(0) === 'R') {
            const pos = parseInt(from.substring(1), 10);
            this.current = this.rewriteRules[pos];
            this.direct = (from.charAt(0) === 'L');
            this.highlight(this.direct ? this.current.left: this.current.right);
            const rwAst = this.direct ? binop("->", this.current.left.ast, this.current.right.ast) : binop("->", this.current.right.ast, this.current.left.ast);
            render('rewrite', rwAst);
        }
    }

    match(term: AST, matcher: AST, subst: Map<string, AST>): boolean {
        if (matcher.kind === Kind.Var) {
            const v = matcher.val;
            if (typeof (v) !== "string") {
                throw new Error("Bad variable");
            }
            const val = subst.get(v);
            if (val !== undefined) {
                return eqTerm(term, val)
            } else {
                subst.set(v, term);
                return true;
            }
        } else {
            if (term.kind === matcher.kind && term.val == matcher.val &&
                term.children.length === matcher.children.length) {
                return term.children.every((te, i) => this.match(te, matcher.children[i], subst));
            } else {
                this.bad(term);
                return false;
            }
        }
        return true;
    }

    computeMode() {
        this.current = null;
        this.highlight(null);
        render('rewrite', variable("\\mathrm{compute}"));
    }

    checkResult(text: string) {
        try {
            if (this.toReduce === null) return;
            const toReduce = compute(this.toReduce);
            const astReduced = parse(text);
            const reduced = compute(astReduced);
            if (reduced !== toReduce) throw new Error("Pas le bon résultat");
            this.toReduce.kind = astReduced.kind;
            this.toReduce.children = astReduced.children;
            this.toReduce.val = astReduced.val;
            nameIt(this.formula, "FO");
            this.setFormula(this.formula);
        } catch(e) {
            if (e instanceof Error) { this.error(e.message); }
            else throw(e);
        }
    }

    error(e: string) {
        $("#error").text(e);
        $("#error-modal").modal()
    }

    highlight(layout: Layout | null) {
        if (this.highlighted !== null) {
            highlight(this.highlighted, 0);
        }
        if (layout !== null) {
            highlight(layout, 1);
        }
        this.highlighted = layout;
    }

    bad(ast: AST) {
        const node = this.layout.tree.select(`#I${ast.id}`)
        effect(node, "red", 2000);
    }
}