{
  function makeInteger(o: string[]): number {
    return parseInt(o.join(""), 10);
  }

  function makeIdent(l:string, o: string[]): string {
    return l + o.join("");
  }
}

start
  = equality

equality
  = left:additive _ "=" _ right:additive { return ast.binop("=", left(null), right(null)); }
  / expr:additive { return expr(null); }

additive
  = left:multiplicative _ "+" _ right:additive { return (d: ast.delayed | null) => right({left: ast.solveLeft(d, left(null)), op: "+" }); }
  / left:multiplicative _ "-" _ right:additive { return (d: ast.delayed | null) => right({left: ast.solveLeft(d, left(null)), op: "-" }); }
  / expr:multiplicative { return (d: ast.delayed | null) => ast.solveLeft(d, expr(null)); }

multiplicative
  = left:negative _ "*" _ right:multiplicative { return (d: ast.delayed | null) => right({left: ast.solveLeft(d, left), op: "*" }); }
  / left:negative _ "/" _ right:multiplicative { return (d: ast.delayed | null) => right({left: ast.solveLeft(d, left), op: "/" }); }
  / expr:negative { return (d: ast.delayed | null) => ast.solveLeft(d, expr); }

negative
 = "-" expr:primary  { return ast.unop("-", expr); }
 / primary

primary
  = val:integer { return ast.constant(val); }
  / vari:identifier { return ast.variable(vari); }
  / "(" _ additive:additive _ ")" { return additive(null); }

identifier = alpha:[a-zA-Z] alphanum:[a-zA-Z0-9]* { return makeIdent(alpha, alphanum)}

integer = digits:[0-9]+ { return makeInteger(digits); }

_ = [ \t\r\n]*
