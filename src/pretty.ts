import {AST, Kind} from "./ast";

enum Level {
    Equality,
    Additive,
    Multiplicative,
    Unary
}

export function latex(ast: AST, level=Level.Equality): string {
    let expr: string;
    if (ast.kind == Kind.Call) {
        switch(ast.val) {
            case "=":
                expr =`${latex(ast.children[0], Level.Additive)} = ${latex(ast.children[1], Level.Additive)}`;
                return (level <= Level.Equality) ? expr : `(${expr})`;
            case "+":
                expr =`${latex(ast.children[0], Level.Additive)} + ${latex(ast.children[1], Level.Multiplicative)}`;
                return (level <= Level.Additive) ? expr : `(${expr})`;
            case "-":
                    expr =`${latex(ast.children[0], Level.Additive)} - ${latex(ast.children[1], Level.Multiplicative)}`;
                    return (level <= Level.Additive) ? expr : `(${expr})`;
            case "*":
                expr =`${latex(ast.children[0], Level.Multiplicative)} \\times ${latex(ast.children[1], Level.Unary)}`;
                return (level <= Level.Multiplicative) ? expr : `(${expr})`;
            case "/":
                expr =`\\dfrac{${latex(ast.children[0], Level.Additive)}}{${latex(ast.children[1],    Level.Additive)}}`;
                return expr;
            case "->":
                expr =`${latex(ast.children[0])} \\;\\mapsto\\; ${latex(ast.children[1])}`;
                return expr;
            default:
                return `???[${ast.val}]`
        }
    } else {
        return ""+ast.val;
    }
}

export function render(sel: string, ast: AST) {
    const math = MathJax.Hub.getAllJax(sel)[0];
    MathJax.Hub.Queue(['Text', math, latex(ast)]);
}
