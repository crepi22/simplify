import { axisTop } from "d3";

export enum Kind { Call, Var, Const }

export interface AST {
    id: number;
    from?: string;
    kind: Kind;
    val: number | string;
    children: AST[];
}

export interface delayed {
    left: AST;
    op: string;
}

export type rightKont = (d: delayed | null) => AST;

let count = 0;

export function binop(op:string, x: AST, y: AST): AST {
    return {id: count++, kind: Kind.Call, val: op, children: [x, y]};
}

export function unop(op:string, x: AST): AST {
    return {id: count++, kind: Kind.Call, val: op, children: [x]};
}

export function variable(v: string): AST {
    return {id: count++, kind: Kind.Var, val: v, children: []};
}

export function constant(v: number) : AST {
    return {id: count++, kind: Kind.Const, val: v, children: []};
}

export function solveLeft(d: delayed | null, expr: AST): AST {
    return (d===null) ? expr : binop(d.op, d.left, expr);
}

export function nameIt(expr: AST, name: string) {
    expr.from = name;
    for(const child of expr.children) {
        nameIt(child, name);
    }
}

export function eqTerm(term1: AST, term2: AST): boolean {
    return term1.kind === term2.kind && term1.val === term2.val &&
        term1.children.every((element,i) => this.eqTerm(element, term2.children[i]));
}

export function copy(term: AST, subst?: Map<string, AST>): AST {
    if (subst !== undefined && term.kind === Kind.Var) {
        const v = subst.get(term.val as string);
        if (v !== undefined) {
            return copy(v);
        }
    }
    return {
        id: count++,
        kind: term.kind,
        val: term.val,
        children: term.children.map((x) => copy(x, subst)),
    }
}
