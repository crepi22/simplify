const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: {
    simplifier: "./src/main.ts",
  },
  mode: "development",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      }
    ]
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "dist")
  },
  performance: { hints: false },
  resolve: {
    alias: {
      "snapsvg": "snapsvg-cjs"
    },
    extensions: [ ".tsx", ".ts", ".js" ]
  },
};
